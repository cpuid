/*
 * Copyright (C) 2015 Jan Kiszka <jan.kiszka@web.de>.
 *
 * cpuid is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

static void cpuid(uint32_t leaf, uint32_t subleaf, uint32_t *eax,
		  uint32_t *ebx, uint32_t *ecx, uint32_t *edx)
{
	uint32_t reg[4];

#ifdef __x86_64__
	asm volatile("cpuid"
		     : "=a"(reg[0]), "=b"(reg[1]), "=c"(reg[2]), "=d"(reg[3])
		     : "0"(leaf), "2"(subleaf) : "cc");
#else
	asm volatile("pusha \n\t"
		     "cpuid \n\t"
		     "mov %%eax, 0(%2) \n\t"
		     "mov %%ebx, 4(%2) \n\t"
		     "mov %%ecx, 8(%2) \n\t"
		     "mov %%edx, 12(%2) \n\t"
		     "popa"
		     : : "a"(leaf), "c"(subleaf), "S"(reg)
		     : "memory", "cc");
#endif
	*eax = reg[0];
	*ebx = reg[1];
	*ecx = reg[2];
	*edx = reg[3];
}

static char alnum(char c)
{
	return isalnum(c) ? c : ' ';
}

static const char *cpuid_str(uint32_t val)
{
	char *buf = malloc(8);

	snprintf(buf, 8, "\'%c%c%c%c\'",
		 alnum(val), alnum(val >> 8),
		 alnum(val >> 16), alnum(val >> 24));
	return buf;
}

int main(int argc, char *argv[])
{
	uint32_t leaf, subleaf = 0, eax, ebx, ecx, edx;

	if (argc < 2) {
		printf("usage: %s LEAF [SUBLEAF]\n", argv[0]);
		exit(1);
	}
	leaf = strtol(argv[1], NULL, 0);
	if (argc > 2)
		subleaf = strtol(argv[2], NULL, 0);

	cpuid(leaf, subleaf, &eax, &ebx, &ecx, &edx);

	printf("cpuid(0x%08x, 0x%08x):\n"
	       "\teax = 0x%08x (%s)\n"
	       "\tebx = 0x%08x (%s)\n"
	       "\tecx = 0x%08x (%s)\n"
	       "\tedx = 0x%08x (%s)\n",
	       leaf, subleaf,
	       eax, cpuid_str(eax),
	       ebx, cpuid_str(ebx),
	       ecx, cpuid_str(ecx),
	       edx, cpuid_str(edx));
	return 0;
}
